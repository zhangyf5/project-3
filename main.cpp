#include <iostream>
#include <random>
#include <time.h>
#include <unistd.h>
#include "platform.hpp"
#include "State.hpp"
#include "Agents.hpp"

void runTask1();
void runTask2();
void runTask3(int);

int main(int argc, char *argv[])
{
    int opt = 0;
    int task = 3;
    int seed = time(NULL);
    int trial_time = 100;
    while ((opt = getopt(argc, argv, "T:s:t:")) != -1) {
        switch (opt)
        {
        case 'T':
            task = atoi(optarg);
            break;
        
        case 's':
            seed = atoi(optarg);
            break;

        case 't':
            trial_time = atoi(optarg);
            break;
        }
    }

    srand(seed);
    
    switch (task)
    {
    case 1:
        runTask1();
        break;
    
    case 2:
        runTask2();
        break;
    
    case 3:
        runTask3(trial_time);
        break;
    
    default:
        std::cout << "No such task! Please try again." << std::endl;
        break;
    }

#ifdef _WIN32
    system("pause");
#endif

    return 0;
}


void runTask1()
{
    system("mode con cols=40 lines=15");

    Game game;
    RandomAgent agent;
    game.print();

    while (!game.isDead())
    {
        State state(game);
        char action = agent.getAction(state);
        if (!game.isPossibleArrow(action))
        {
            std::cout << "Warning: Illegal Action !" << std::endl;
            exit(1);
        }
        
        game.Move(action);
        game.RandomAdd();
        game.print();
    }
}

void runTask2()
{
    Game game;
    State state(game);
    int score = __testRandomPlayGameOnce(state);
    std::cout << "The score of this random game is " << score << std::endl;
}

void runTask3(int trial_time)
{
    system("mode con cols=40 lines=15");

    Game game;
    MonteCarloAgent agent;
    agent.setTrialTime(trial_time);
    game.print();

    while (!game.isDead())
    {
        State state(game);
        char action = agent.getAction(state);
        if (!game.isPossibleArrow(action))
        {
            std::cout << "Warning: Illegal Action !" << std::endl;
            exit(1);
        }
        
        game.Move(action);
        game.RandomAdd();
        game.print();
    }
}
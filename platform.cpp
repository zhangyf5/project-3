#include <iostream>
#include "platform.hpp"

/* ****************** CLASS IMPLEMENTATION ********************* *
 * Welcome to C plus plus (C++) !                                *
 *                                                               *
 * These functions inplements the class Game. Your task is to    *
 * read them carefully and find the missing prototypes           *
 * in platform.hpp                                               *
 *                                                               *
 * ************************************************************* */

Game::Game() : score(0)
{
    /* Constructor. Init the chess board with 2 elements. 
     */
    RandomAdd();
}

Game::Game(int chess[4][4]) : score(0)
{
    /* Construstor. Init the chess board same with 'chess'. Use 
     * copy instead of inplace method. i.e. if the original chess
     * data changes afterwards, it does not affect the chess board.
     */
    for (int i = 0; i < 4; ++i)
    {
        for (int j = 0; j < 4; ++j)
        {
            this->chess[i][j] = chess[i][j];
        }
    }
}

void Game::RandomAdd()
{
    /* Add 2 or 4 in the chess board.
     * 2 has probability 5/6 and 4 has probability 1/6.
     * If the chess board is empty, add 2 numbers.
     * If the chess board is full, do nothing.
     * In other cases, add one number in the empty place.
     */
    int space = Space();
    int num = space ? (space>15 ? 2 : 1) : 0;

    for (int i = 0; i < num; ++i)
    {
        int t = rand() % 6;
        int a = rand() % 16;
        while (chess[a / 4][a % 4])
            a = rand() % 16;

        chess[a / 4][a % 4] = t ? 2 : 4;
    }
}

int Game::Space()
{
    /* Return the number of spaces in the chess board. 
     */
    int m = 0;
    for (int i = 0; i < 4; ++i)
    {
        for (int j = 0; j < 4; ++j)
        {
            if (chess[i][j] == 0)
            {
                ++m;
            }
        }
    }
    return m;
}

int Game::BasicMove(int line[])
{
    /* Performing a move in one line. The direction is merge to the left.
     * The operation is taken in place. i.e. it changes the 
     * parameter 'line'.
     * The return value is the score produced by this line
     * operation.
     * 
     * EXAMPLE:
     *   INPUT: line = [2, 4, 4, 0]
     *  OUTPUT: line = [2, 8, 0, 0]
     *          return 8
     */
    int cur_score = 0;
    for (int i = 0; i < 3; ++i)
    {
        int sum = 0;
        for (int j = i + 1; j < 4; ++j)
            sum = sum + line[j];
        while ((line[i] == 0) && (sum))
        {
            for (int j = i; j < 3; ++j)
            {
                line[j] = line[j + 1];
                line[j + 1] = 0;
            }
        }
    }

    if (line[0] == line[1])
    {
        if (line[2] == line[3])
        {
            line[0] = line[0] * 2;
            line[1] = line[2] * 2;
            line[2] = 0;
            line[3] = 0;
            cur_score = line[0] + line[1];
        }
        else
        {
            line[0] = line[0] * 2;
            line[1] = line[2];
            line[2] = line[3];
            line[3] = 0;
            cur_score = line[0];
        }
    }
    else if (line[1] == line[2])
    {
        line[1] = line[1] * 2;
        line[2] = line[3];
        line[3] = 0;
        cur_score = line[1];
    }
    else if (line[2] == line[3])
    {
        line[2] = line[2] * 2;
        line[3] = 0;
        cur_score = line[2];
    }
    return cur_score;
}

void Game::Move(char arrow)
{
    /* Do a chess board move operation to 'chess' based on the
     * action 'arrow'. 
     * Change 'chess' in the class directly, no return value.
     */
    if (arrow == 'A')
    {
        for (int i = 0; i < 4; ++i)
            score += BasicMove(chess[i]);
    }
    else if (arrow == 'D')
    {
        for (int i = 0; i < 4; ++i)
        {
            int temp[4] = {};
            for (int j = 0; j < 4; ++j)
                temp[j] = chess[i][3 - j];
            score += BasicMove(temp);
            for (int j = 0; j < 4; ++j)
                chess[i][3 - j] = temp[j];
        }
    }
    else if (arrow == 'W')
    {
        for (int i = 0; i < 4; ++i)
        {
            int temp[4] = {};
            for (int j = 0; j < 4; ++j)
                temp[j] = chess[j][i];
            score += BasicMove(temp);
            for (int j = 0; j < 4; ++j)
                chess[j][i] = temp[j];
        }
    }
    else if (arrow == 'S')
    {
        for (int i = 0; i < 4; ++i)
        {
            int temp[4] = {};
            for (int j = 0; j < 4; ++j)
                temp[3 - j] = chess[j][i];
            score += BasicMove(temp);
            for (int j = 0; j < 4; ++j)
                chess[j][i] = temp[3 - j];
        }
    }
}

bool Game::isDead()
{
    /* Return if the game is over.
     * If no action is legal to take, then return true.
     * Else return false.
     * The implementation is comparing the neighours and 
     * make sure that no same value occuring.
     */
    if (Space())
        return false;
    for (int i = 0; i < 4; ++i)
    {
        for (int j = 0; j < 3; ++j)
            if (chess[i][j] == chess[i][j + 1])
                return false;
    }
    for (int j = 0; j < 4; ++j)
    {
        for (int i = 0; i < 3; ++i)
            if (chess[i][j] == chess[i + 1][j])
                return false;
    }
    return true;
}

bool Game::isPossibleArrow(char arrow)
{
    /* Return if an action 'arrow' is legel.
     */
    Game temp(chess);
    temp.Move(arrow);
    for (int i = 0; i < 4; ++i)
    {
        for (int j = 0; j < 4; ++j)
        {
            if (this->chess[i][j] != temp.GetChess()[i][j])
                return true;
        }
    }
    return false;
}

chess_type
Game::GetChess()
{
    /* Return the chess board.
     */
    return chess;
}

int Game::GetScore()
{
    /* Return the score value.
     */
    return score;
}

int Game::GetMax()
{
    /* Return the max value in the chess board.
     */
    int max_num = 0;
    for (int i = 0; i < 4; ++i)
        for (int j = 0; j < 4; ++j)
            if (max_num < chess[i][j])
                max_num = chess[i][j];
    return max_num;
}

/* ********************** PRINT LINES ************************** *
 * Print the chess board.                                        *
 * You do not have to read the lines below.                      *
 * ************************************************************* */

void clear_screen()
{
#ifdef _WIN32
    COORD pos = {0, 0};
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), pos);
    CONSOLE_CURSOR_INFO info = {1, 0};
    SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &info);
#else
    printf("\033c");
    printf("\033[?25l");
#endif
}

void Game::print()
{
    clear_screen();
    std::cout << std::endl
        << "    SCORE: " << std::setw(13) << std::setiosflags(std::ios::left) << score
        << "MAX: " << GetMax() << std::endl
        << std::endl;
    std::cout << "    +------+------+------+------+" << std::endl
        << "    ";
    for (int i = 0; i < 4; ++i)
    {

        for (int j = 0; j < 4; ++j)
        {

            if (chess[i][j])
                std::cout << '|' << std::setw(6) << std::setiosflags(std::ios::left) << chess[i][j];
            else
                std::cout << '|' << "      ";
        }
        std::cout << '|' << std::endl;
        std::cout << "    +------+------+------+------+" << std::endl
            << "    ";
    }
}
